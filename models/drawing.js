const mongoose = require('mongoose');

const DrawingSchema = mongoose.Schema({
    username: {
        type: String,
    },
    drawing: {
        type: [String]
    },
    title: {
        type: String
    }
});

const Drawing = module.exports = mongoose.model('Drawing', DrawingSchema);

module.exports.createDrawing = function(newDrawing, callback){
    newDrawing.save(callback);
}

module.exports.drawingInfo = function(username, callback) {
    let query = {username: username};
    Drawing.find(query, callback);
}
