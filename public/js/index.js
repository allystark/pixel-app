// entry for frontend js
console.log("I'm working");
//define grid parameter - to be put in UI 
const rowSize = 16; 
const gridSize = rowSize ** 2;
const mainGrid = document.querySelector(".grid");
mainGrid.style.gridTemplateColumns = `repeat(${rowSize}, 1fr)`;
mainGrid.style.gridTemplateRows = `repeat(${rowSize}, 1fr)`;
mainGrid.style.width = "500px"; 
mainGrid.style.height = "500px";
//define empty grid array on loading
let gridArray = {}; 
const color = document.querySelector(".color");
// variable to control when slider changes
let isDown = false;
//initial drawing colour - need to add black and white to palette
drawColor = "hsl(0,100%,50%)";
//saved colour from local Storage
const savedColor = localStorage.getItem("color");
//hue value - corresponds to slider values
let hue = 180; 
const colorPicker = document.querySelector(".colorPicker");
//if previous grids exist in local storage, then set the colour picker parameters and the drawing colour to these values
 
if (localStorage.getItem("color")) {
  color.style.backgroundColor = savedColor;
  drawColor = savedColor;
  colorPicker.value = localStorage.getItem("hue");
}
//render initial grid based on parameters at top of page
function initialGrid() {
  //set the gridArray object to be empty
  gridArray = {};
  // create blank pixel grid
  for (let i = 1; i <= gridSize; i += 1) {
    const gridDiv = document.createElement("div");
    gridDiv.id = `gridDiv${i}`;
    gridDiv.className += "subGrid";
    mainGrid.appendChild(gridDiv);
    gridDiv.style.backgroundColor = "rgb(255,255,255)";
    //add pixel id and colour to the gridArray object
    gridArray[`gridDiv${i}`] = (gridDiv.style.backgroundColor);
    //at the end of the loop, save the current grid array object in local storage
    if (i == gridSize) {
      localStorage.setItem("grid", JSON.stringify(gridArray));
    }
  }
}
//load previous grid from localstorage
function loadDraft() {
  gridArray = JSON.parse(localStorage.getItem("grid"));
  for (let i = 1; i <= gridSize; i += 1) {
    const gridDiv = document.createElement("div");
    gridDiv.id = `gridDiv${i}`;
    gridDiv.className += "subGrid";
    mainGrid.appendChild(gridDiv);
    gridDiv.style.backgroundColor = gridArray[gridDiv.id];
  }
}
//run function depending on whether previous drawings were kept in localstorage
if (!(localStorage.getItem("grid"))) {
  initialGrid();
}
else {
  loadDraft();
}
// function to control the colour slider, indluding setting the hue value and the background colour of the colour bar at the top of the page and the draw color - want to edit the colour picker so it displays the range of colours
function colourSlide(e) {
  if (isDown) {
    const hue = e.target.value;
    const hsla = `hsl(${hue}, 100%, 50%)`
    color.style.backgroundColor = hsla;
    drawColor = hsla;
    //set local storage colour values for reloading
    localStorage.setItem("color", drawColor)
    localStorage.setItem("hue", colorPicker.value);
  }
}
//event listeners for colorPicker to fire function
colorPicker.addEventListener("mousedown", () => isDown = true);
colorPicker.addEventListener("mouseup", () => isDown = false);
colorPicker.addEventListener("mouseleave", () => isDown = false);
colorPicker.addEventListener("mousemove", colourSlide);
//function to reset grid and colours to initial loading states
function resetGrid() {
  pixels.forEach(pixel => {
    pixel.style.backgroundColor = "rgb(255,255,255)"
    gridArray[pixel.id] = pixel.style.backgroundColor
  });
  colorPicker.value = 0;
  localStorage.clear();
  drawColor = "hsl(0,100%,50%)";
  color.style.backgroundColor = "hsl(0,100%,50%)";
}

//function to draw pixels and colours to grid, and update the objects and strings in local storage
function draw() {
  console.log(drawColor);
  console.log(this);
  localStorage.setItem("color", drawColor)
  localStorage.setItem("hue", colorPicker.value);
  this.style.backgroundColor = drawColor;
  gridArray[this.id] = this.style.backgroundColor;
  localStorage.setItem("grid", JSON.stringify(gridArray));
}
//draw on click 
const pixels = document.querySelectorAll(".subGrid");
pixels.forEach(pixel => pixel.addEventListener("click", draw));

// dummy array for loading - run load() in console to test
const gridArray2 =
  {
    gridDiv1: "rgb(255, 255, 255)",
    gridDiv2: "rgb(255, 255, 255)",
    gridDiv3: "rgb(255, 255, 255)",
    gridDiv4: "rgb(255, 255, 255)",
    gridDiv5: "rgb(255, 255, 255)",
    gridDiv6: "rgb(255, 255, 255)",
    gridDiv7: "rgb(255, 255, 255)",
    gridDiv8: "rgb(255, 255, 255)",
    gridDiv9: "rgb(255, 255, 255)",
    gridDiv10: "rgb(255, 255, 255)",
    gridDiv11: "rgb(255, 255, 255)",
    gridDiv12: "rgb(255, 255, 255)",
    gridDiv13: "rgb(255, 255, 255)",
    gridDiv14: "rgb(255, 255, 255)",
    gridDiv15: "rgb(255, 255, 255)",
    gridDiv16: "rgb(255, 255, 255)",
    gridDiv17: "rgb(255, 255, 255)",
    gridDiv18: "rgb(255, 255, 255)",
    gridDiv19: "rgb(255, 255, 255)",
    gridDiv20: "rgb(255, 255, 255)",
    gridDiv21: "rgb(255, 255, 255)",
    gridDiv22: "rgb(255, 255, 255)",
    gridDiv23: "rgb(255, 255, 255)",
    gridDiv24: "rgb(255, 255, 255)",
    gridDiv25: "rgb(255, 255, 255)",
    gridDiv26: "rgb(255, 255, 255)",
    gridDiv27: "rgb(255, 255, 255)",
    gridDiv28: "rgb(255, 255, 255)",
    gridDiv29: "rgb(255, 255, 255)",
    gridDiv30: "rgb(255, 255, 255)",
    gridDiv31: "rgb(255, 255, 255)",
    gridDiv32: "rgb(255, 255, 255)",
    gridDiv33: "rgb(255, 255, 255)",
    gridDiv34: "rgb(255, 255, 255)",
    gridDiv35: "rgb(255, 255, 255)",
    gridDiv36: "rgb(255, 255, 255)",
    gridDiv37: "rgb(255, 255, 255)",
    gridDiv38: "rgb(255, 255, 255)",
    gridDiv39: "rgb(255, 255, 255)",
    gridDiv40: "rgb(255, 255, 255)",
    gridDiv41: "rgb(255, 255, 255)",
    gridDiv42: "rgb(255, 255, 255)",
    gridDiv43: "rgb(255, 255, 255)",
    gridDiv44: "rgb(255, 255, 255)",
    gridDiv45: "rgb(255, 255, 255)",
    gridDiv46: "rgb(255, 255, 255)",
    gridDiv47: "rgb(255, 255, 255)",
    gridDiv48: "rgb(255, 255, 255)",
    gridDiv49: "rgb(255, 255, 255)",
    gridDiv50: "rgb(255, 255, 255)",
    gridDiv51: "rgb(255, 255, 255)",
    gridDiv52: "rgb(255, 255, 255)",
    gridDiv53: "rgb(255, 255, 255)",
    gridDiv54: "rgb(255, 255, 255)",
    gridDiv55: "rgb(255, 255, 255)",
    gridDiv56: "rgb(255, 255, 255)",
    gridDiv57: "rgb(255, 255, 255)",
    gridDiv58: "rgb(255, 255, 255)",
    gridDiv59: "rgb(255, 255, 255)",
    gridDiv60: "rgb(255, 255, 255)",
    gridDiv61: "rgb(255, 255, 255)",
    gridDiv62: "rgb(255, 255, 255)",
    gridDiv63: "rgb(255, 255, 255)",
    gridDiv64: "rgb(255, 255, 255)",
    gridDiv65: "rgb(255, 255, 255)",
    gridDiv66: "rgb(255, 255, 255)",
    gridDiv67: "rgb(255, 255, 255)",
    gridDiv68: "rgb(255, 255, 255)",
    gridDiv69: "rgb(255, 255, 255)",
    gridDiv70: "rgb(255, 255, 255)",
    gridDiv71: "rgb(255, 255, 255)",
    gridDiv72: "rgb(255, 255, 255)",
    gridDiv73: "rgb(255, 255, 255)",
    gridDiv74: "rgb(255, 255, 255)",
    gridDiv75: "rgb(255, 255, 255)",
    gridDiv76: "rgb(255, 255, 255)",
    gridDiv77: "rgb(255, 255, 255)",
    gridDiv78: "rgb(255, 255, 255)",
    gridDiv79: "rgb(255, 255, 255)",
    gridDiv80: "rgb(255, 255, 255)",
    gridDiv81: "rgb(255, 255, 255)",
    gridDiv82: "rgb(255, 255, 255)",
    gridDiv83: "rgb(255, 255, 255)",
    gridDiv84: "rgb(255, 255, 255)",
    gridDiv85: "rgb(255, 255, 255)",
    gridDiv86: "rgb(0, 0, 0)",
    gridDiv87: "rgb(255, 255, 255)",
    gridDiv88: "rgb(255, 255, 255)",
    gridDiv89: "rgb(255, 255, 255)",
    gridDiv90: "rgb(255, 255, 255)",
    gridDiv91: "rgb(0, 0, 0)",
    gridDiv92: "rgb(255, 255, 255)",
    gridDiv93: "rgb(255, 255, 255)",
    gridDiv94: "rgb(255, 255, 255)",
    gridDiv95: "rgb(255, 255, 255)",
    gridDiv96: "rgb(255, 255, 255)",
    gridDiv97: "rgb(255, 255, 255)",
    gridDiv98: "rgb(255, 255, 255)",
    gridDiv99: "rgb(255, 255, 255)",
    gridDiv100: "rgb(255, 255, 255)",
    gridDiv101: "rgb(255, 255, 255)",
    gridDiv102: "rgb(255, 255, 255)",
    gridDiv103: "rgb(255, 255, 255)",
    gridDiv104: "rgb(255, 255, 255)",
    gridDiv105: "rgb(255, 255, 255)",
    gridDiv106: "rgb(255, 255, 255)",
    gridDiv107: "rgb(255, 255, 255)",
    gridDiv108: "rgb(255, 255, 255)",
    gridDiv109: "rgb(255, 255, 255)",
    gridDiv110: "rgb(255, 255, 255)",
    gridDiv111: "rgb(255, 255, 255)",
    gridDiv112: "rgb(255, 255, 255)",
    gridDiv113: "rgb(255, 255, 255)",
    gridDiv114: "rgb(255, 255, 255)",
    gridDiv115: "rgb(255, 255, 255)",
    gridDiv116: "rgb(255, 255, 255)",
    gridDiv117: "rgb(255, 255, 255)",
    gridDiv118: "rgb(255, 255, 255)",
    gridDiv119: "rgb(255, 255, 255)",
    gridDiv120: "rgb(255, 255, 255)",
    gridDiv121: "rgb(255, 255, 255)",
    gridDiv122: "rgb(255, 255, 255)",
    gridDiv123: "rgb(255, 255, 255)",
    gridDiv124: "rgb(255, 255, 255)",
    gridDiv125: "rgb(255, 255, 255)",
    gridDiv126: "rgb(255, 255, 255)",
    gridDiv127: "rgb(255, 255, 255)",
    gridDiv128: "rgb(255, 255, 255)",
    gridDiv129: "rgb(255, 255, 255)",
    gridDiv130: "rgb(255, 255, 255)",
    gridDiv131: "rgb(255, 255, 255)",
    gridDiv132: "rgb(255, 255, 255)",
    gridDiv133: "rgb(255, 255, 255)",
    gridDiv134: "rgb(255, 255, 255)",
    gridDiv135: "rgb(255, 255, 255)",
    gridDiv136: "rgb(255, 255, 255)",
    gridDiv137: "rgb(255, 255, 255)",
    gridDiv138: "rgb(255, 255, 255)",
    gridDiv139: "rgb(255, 255, 255)",
    gridDiv140: "rgb(255, 255, 255)",
    gridDiv141: "rgb(255, 255, 255)",
    gridDiv142: "rgb(255, 255, 255)",
    gridDiv143: "rgb(255, 255, 255)",
    gridDiv144: "rgb(255, 255, 255)",
    gridDiv145: "rgb(255, 255, 255)",
    gridDiv146: "rgb(255, 255, 255)",
    gridDiv147: "rgb(255, 255, 255)",
    gridDiv148: "rgb(255, 255, 255)",
    gridDiv149: "rgb(0, 0, 0)",
    gridDiv150: "rgb(255, 255, 255)",
    gridDiv151: "rgb(255, 255, 255)",
    gridDiv152: "rgb(255, 255, 255)",
    gridDiv153: "rgb(255, 255, 255)",
    gridDiv154: "rgb(255, 255, 255)",
    gridDiv155: "rgb(255, 255, 255)",
    gridDiv156: "rgb(0, 0, 0)",
    gridDiv157: "rgb(255, 255, 255)",
    gridDiv158: "rgb(255, 255, 255)",
    gridDiv159: "rgb(255, 255, 255)",
    gridDiv160: "rgb(255, 255, 255)",
    gridDiv161: "rgb(255, 255, 255)",
    gridDiv162: "rgb(255, 255, 255)",
    gridDiv163: "rgb(255, 255, 255)",
    gridDiv164: "rgb(255, 255, 255)",
    gridDiv165: "rgb(255, 255, 255)",
    gridDiv166: "rgb(0, 0, 0)",
    gridDiv167: "rgb(255, 255, 255)",
    gridDiv168: "rgb(255, 255, 255)",
    gridDiv169: "rgb(255, 255, 255)",
    gridDiv170: "rgb(255, 255, 255)",
    gridDiv171: "rgb(0, 0, 0)",
    gridDiv172: "rgb(255, 255, 255)",
    gridDiv173: "rgb(255, 255, 255)",
    gridDiv174: "rgb(255, 255, 255)",
    gridDiv175: "rgb(255, 255, 255)",
    gridDiv176: "rgb(255, 255, 255)",
    gridDiv177: "rgb(255, 255, 255)",
    gridDiv178: "rgb(255, 255, 255)",
    gridDiv179: "rgb(255, 255, 255)",
    gridDiv180: "rgb(255, 255, 255)",
    gridDiv181: "rgb(255, 255, 255)",
    gridDiv182: "rgb(255, 255, 255)",
    gridDiv183: "rgb(0, 0, 0)",
    gridDiv184: "rgb(0, 0, 0)",
    gridDiv185: "rgb(0, 0, 0)",
    gridDiv186: "rgb(0, 0, 0)",
    gridDiv187: "rgb(255, 255, 255)",
    gridDiv188: "rgb(255, 255, 255)",
    gridDiv189: "rgb(255, 255, 255)",
    gridDiv190: "rgb(255, 255, 255)",
    gridDiv191: "rgb(255, 255, 255)",
    gridDiv192: "rgb(255, 255, 255)",
    gridDiv193: "rgb(255, 255, 255)",
    gridDiv194: "rgb(255, 255, 255)",
    gridDiv195: "rgb(255, 255, 255)",
    gridDiv196: "rgb(255, 255, 255)",
    gridDiv197: "rgb(255, 255, 255)",
    gridDiv198: "rgb(255, 255, 255)",
    gridDiv199: "rgb(255, 255, 255)",
    gridDiv200: "rgb(255, 255, 255)",
    gridDiv201: "rgb(255, 255, 255)",
    gridDiv202: "rgb(255, 255, 255)",
    gridDiv203: "rgb(255, 255, 255)",
    gridDiv204: "rgb(255, 255, 255)",
    gridDiv205: "rgb(255, 255, 255)",
    gridDiv206: "rgb(255, 255, 255)",
    gridDiv207: "rgb(255, 255, 255)",
    gridDiv208: "rgb(255, 255, 255)",
    gridDiv209: "rgb(255, 255, 255)",
    gridDiv210: "rgb(255, 255, 255)",
    gridDiv211: "rgb(255, 255, 255)",
    gridDiv212: "rgb(255, 255, 255)",
    gridDiv213: "rgb(255, 255, 255)",
    gridDiv214: "rgb(255, 255, 255)",
    gridDiv215: "rgb(255, 255, 255)",
    gridDiv216: "rgb(255, 255, 255)",
    gridDiv217: "rgb(255, 255, 255)",
    gridDiv218: "rgb(255, 255, 255)",
    gridDiv219: "rgb(255, 255, 255)",
    gridDiv220: "rgb(255, 255, 255)",
    gridDiv221: "rgb(255, 255, 255)",
    gridDiv222: "rgb(255, 255, 255)",
    gridDiv223: "rgb(255, 255, 255)",
    gridDiv224: "rgb(255, 255, 255)",
    gridDiv225: "rgb(255, 255, 255)",
    gridDiv226: "rgb(255, 255, 255)",
    gridDiv227: "rgb(255, 255, 255)",
    gridDiv228: "rgb(255, 255, 255)",
    gridDiv229: "rgb(255, 255, 255)",
    gridDiv230: "rgb(255, 255, 255)",
    gridDiv231: "rgb(255, 255, 255)",
    gridDiv232: "rgb(255, 255, 255)",
    gridDiv233: "rgb(255, 255, 255)",
    gridDiv234: "rgb(255, 255, 255)",
    gridDiv235: "rgb(255, 255, 255)",
    gridDiv236: "rgb(255, 255, 255)",
    gridDiv237: "rgb(255, 255, 255)",
    gridDiv238: "rgb(255, 255, 255)",
    gridDiv239: "rgb(255, 255, 255)",
    gridDiv240: "rgb(255, 255, 255)",
    gridDiv241: "rgb(255, 255, 255)",
    gridDiv242: "rgb(255, 255, 255)",
    gridDiv243: "rgb(255, 255, 255)",
    gridDiv244: "rgb(255, 255, 255)",
    gridDiv245: "rgb(255, 255, 255)",
    gridDiv246: "rgb(255, 255, 255)",
    gridDiv247: "rgb(255, 255, 255)",
    gridDiv248: "rgb(255, 255, 255)",
    gridDiv249: "rgb(255, 255, 255)",
    gridDiv250: "rgb(255, 255, 255)",
    gridDiv251: "rgb(255, 255, 255)",
    gridDiv252: "rgb(255, 255, 255)",
    gridDiv253: "rgb(255, 255, 255)",
    gridDiv254: "rgb(255, 255, 255)",
    gridDiv255: "rgb(255, 255, 255)",
    gridDiv256: "rgb(255, 255, 255)"
  }

function load() {
  pixels.forEach((pixel) => {
    const pixelLoad = document.querySelector(`#${pixel.id}`);
    pixelLoad.style.backgroundColor = gridArray2[pixel.id];
    console.log(pixel.id);
  });
}
