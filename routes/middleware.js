const passport = require('passport');

module.exports.ensureAuthenticated = function(req, res, next){
    if(req.isAuthenticated()){
        return next();
    } else {
        req.method === 'POST' ? res.status(403).end('not authorized') : res.redirect('/users/login');
    }
}
