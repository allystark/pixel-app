const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
// routes
const users = require('./routes/users');
const drawing = require('./routes/draw');
//create instance of express
const app = express();
//create viewing engine
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({defaultLayout: 'layout'}));
app.set('view engine', 'handlebars');
//bodyparser middleware
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
//static folder
app.use(express.static(path.join(__dirname, 'public')));
//express session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));
app.use(passport.initialize());
app.use(passport.session());
//express validator
app.use(expressValidator({
    errorFormatter: (param, msg, value) => {
        var namespace = param.split('.'),
        root = naqmespace.shift(),
        formParam = root;

        while(namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));
app.use(expressValidator({
    customValidators: {
        isRightSize: (value) => {
            var arr = value.split('/');
            return arr.length > 1;
        }
    }
}));
//connect Flash
app.use(flash());
//Global vars
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});
app.use('/', users);
app.use('/users', users);
app.use('/draw', drawing);
//connect to mongod
//for heroku
//mongoose.connect(process.env.MONGODB_URI);
//local
mongoose.connect('mongodb://mydatabase:12345@ds121896.mlab.com:21896/pixelart', () => {
    console.log('Connected to DB!');
});
//listen on port
const port = 8080;

app.listen(process.env.PORT || port, () => {
    console.log("listening on http://localhost:" + port);
}); 
