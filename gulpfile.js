'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const nodemon = require('gulp-nodemon');
const browserSync = require('browser-sync');
const reload = browserSync.reload;
const packageJSON = require('./package.json');

gulp.task('nodemon', function (cb) {

  let started = false;

  return nodemon({
    script : packageJSON.main,
    ignore: ['public'],
  }).on('start', function () {
    if (!started) {
      cb();
      started = true;
    }
  });
})

gulp.task('browser-sync', ['nodemon'], function() {
  browserSync.init({
    proxy: 'localhost:8080',
    files: ["public/js/*.js", "public/css/*.css", "views/layouts/*.handlebars",
     "views/*.handlebars"]
  });
});

gulp.task('sass', function () {
  return gulp.src('./public/sass/style.scss')
      .pipe(sass({
        errLogToConsole : true,
        sourceComments : true,
      }).on('error', sass.logError))
      .pipe(gulp.dest('./public/css'))
      .pipe(reload({ stream : true })); 
});

gulp.task('watch', function () {
  gulp.watch('./public/sass/*.scss', ['sass']);
}); 

gulp.task('default', ['watch', 'browser-sync']);