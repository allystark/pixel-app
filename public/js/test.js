let arr = new Array(256);
arr.fill('#0000ff');

let title = 'test drawing';

$('#test').on('click', () => {
    axios.post('/draw/save', {
        title: title,
        grid: arr
    })
    .then((res) => {
        console.log(res);
    })
    .catch((err) => {
        console.log(err);
    });
});

