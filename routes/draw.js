const express = require('express');
const router = express.Router();
const Drawing = require('../models/drawing');
const middleware = require('./middleware');

router.get('/view', middleware.ensureAuthenticated, function(req, res){
    let username = res.locals.user.username;
    Drawing.drawingInfo(username, (err, data) => {
        res.render('view', {
            data: data
        });
    });
});

router.get('/draw',middleware.ensureAuthenticated, function(req, res){
    let username = res.locals.user.username;
    Drawing.drawingInfo(username, (err, data) => {
        let names = [];
        for (let i = 0; i < data.length; i++) {
            names.push({
                title: data[i].title,
                id: data[i]._id
            });
        }
        res.render('draw', {
            names: names
        });
    });
});

router.post('/save',middleware.ensureAuthenticated, (req, res) => {
    let username = res.locals.user.username;
    let drawingTitle = req.body.title;
    let drawingGrid = req.body.grid;
    
    let newDrawing = new Drawing({
        username: username,
        drawing: drawingGrid,
        title: drawingTitle
    });

    Drawing.createDrawing(newDrawing, (err, drawing) => {
       if(err) {
           res.status(500).end('Error saving');
       }
        res.status(200).end('Saved the masterwork ' + drawing.title + " by the renowned artist " + drawing.username);
    });
});
//TODO get one grid for editing
//middleware.ensureAuthenticated
router.post('/loadOne', (req, res) => {
    console.log(req.body)
    Drawing.findById(req.body.id, (err, data) => {
        res.end();
    });
});

module.exports = router;
